//
//  ViewController.m
//  ScrollViewer
//
//  Created by Carissa on 1/9/18.
//  Copyright © 2018 Carissa. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIScrollView* scrollview = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        //Initalizing scrollview memory and allocating, and setting frame
    
    scrollview.contentSize = CGSizeMake(self.view.frame.size.width * 3, self.view.frame.size.height*3);
    scrollview.backgroundColor = [UIColor whiteColor];
    
    [scrollview setPagingEnabled:YES];
    [self.view addSubview:scrollview];
    
    
    // 1st column, 1st row View
    UIView* scrollviewer1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
        scrollviewer1.backgroundColor = [UIColor blueColor];
        [scrollview addSubview:scrollviewer1];
    
    // 2nd column, 1st row View
    UIView* scrollviewer2 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width*1, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
        scrollviewer2.backgroundColor = [UIColor redColor];
        [scrollview addSubview:scrollviewer2];
    
    // 3rd column, 1st row View
    UIView* scrollviewer3 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width*2, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
        scrollviewer3.backgroundColor = [UIColor yellowColor];
        [scrollview addSubview:scrollviewer3];
    
    
    
    
    // 1st column, 2nd row View
    UIView* scrollviewer4 = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height*1, self.view.frame.size.width, self.view.frame.size.height)];
    
        scrollviewer4.backgroundColor = [UIColor whiteColor];
        [scrollview addSubview:scrollviewer4];
    
    // 2nd column, 2nd row View
    UIView* scrollviewer5 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width*1, self.view.frame.size.height*1, self.view.frame.size.width, self.view.frame.size.height)];
    
        scrollviewer5.backgroundColor = [UIColor purpleColor];
        [scrollview addSubview:scrollviewer5];
    
    // 3rd column, 2nd row View
    UIView* scrollviewer6 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width*2, self.view.frame.size.height*1, self.view.frame.size.width, self.view.frame.size.height)];
    
        scrollviewer6.backgroundColor = [UIColor orangeColor];
        [scrollview addSubview:scrollviewer6];
    
    
    
    
    
    // 1st column, 3rd row View
    UIView* scrollviewer7 = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height*2, self.view.frame.size.width, self.view.frame.size.height)];
    
        scrollviewer7.backgroundColor = [UIColor blackColor];
        [scrollview addSubview:scrollviewer7];
    
    // 2nd column, 3rd row View
    UIView* scrollviewer8 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width*1, self.view.frame.size.height*2, self.view.frame.size.width, self.view.frame.size.height)];
    
        scrollviewer8.backgroundColor = [UIColor orangeColor];
        [scrollview addSubview:scrollviewer8];
    
    // 3rd column, 3rd row View
    UIView* scrollviewer9 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width*2, self.view.frame.size.height*2, self.view.frame.size.width, self.view.frame.size.height)];
    
        scrollviewer9.backgroundColor = [UIColor greenColor];
        [scrollview addSubview:scrollviewer9];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
