//
//  AppDelegate.h
//  ScrollViewer
//
//  Created by Carissa on 1/9/18.
//  Copyright © 2018 Carissa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

